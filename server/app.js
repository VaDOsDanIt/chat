const WebSocket = require("ws");

const server = new WebSocket.Server({port: process.env.PORT});

server.on('connection', (ws) => {
   ws.send('Socket is connected');

    ws.on('message', message => {
        console.log(message)
        server.clients.forEach(item => {
            if (item.readyState === WebSocket.OPEN) {
                item.send(message);
            }
        });
    })
});

